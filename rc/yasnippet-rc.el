(yas-global-mode 1)

(setq yas-snippet-dirs (append '("~/.emacs.d/snippets")
                               yas-snippet-dirs))
(define-key yas-minor-mode-map (kbd "<tab>") nil)
(define-key yas-minor-mode-map (kbd "TAB") nil)
(define-key yas-minor-mode-map (kbd "<C-tab>") 'yas-expand)

