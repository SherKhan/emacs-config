(add-hook 'c-mode-hook 'helm-gtags-mode)
(add-hook 'c++-mode-hook 'helm-gtags-mode)

(eval-after-load "helm-gtags"
  '(progn
     (define-key helm-gtags-mode-map (kbd "C-c g a") 'helm-gtags-tags-in-this-function)
     (define-key helm-gtags-mode-map (kbd "C-c g s") 'helm-gtags-select)
     (define-key helm-gtags-mode-map (kbd "C-c g f") 'helm-gtags-find-tag-other-window)
     (define-key helm-gtags-mode-map (kbd "C-c g r") 'helm-gtags-find-rtag)
     (define-key helm-gtags-mode-map (kbd "C-c g w") 'helm-gtags-show-stack)
     (define-key helm-gtags-mode-map (kbd "M-.") 'helm-gtags-dwim)
     (define-key helm-gtags-mode-map (kbd "M-,") 'helm-gtags-pop-stack)
     (define-key helm-gtags-mode-map (kbd "C-c <") 'helm-gtags-previous-history)
     (define-key helm-gtags-mode-map (kbd "C-c >") 'helm-gtags-next-history)))
