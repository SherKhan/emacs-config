
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(add-to-list 'load-path "~/.emacs.d/")
(load "package")
(require 'package)
(setq package-archives '(;;("gnu" . "http://elpa.gnu.org/packages/")
                         ;;("marmalade" . "http://marmalade-repo.org/packages/")
                          ("melpa" . "https://melpa.org/packages/")))

(windmove-default-keybindings)

(load-theme 'wombat t)
(tool-bar-mode -1)

(global-set-key "\C-x\C-b" 'ibuffer)

(add-to-list 'auto-mode-alist
               '("\\.\\(?:gemspec\\|irbrc\\|gemrc\\|rake\\|rb\\|ru\\|thor\\)\\'" . ruby-mode))
(add-to-list 'auto-mode-alist
	     '("\\(Capfile\\|Gemfile\\(?:\\.[a-zA-Z0-9._-]+\\)?\\|[rR]akefile\\)\\'" . ruby-mode))

(add-to-list 'auto-mode-alist
	     '("\\(Assfile\\)\\'" . yaml-mode))

(setq-default indent-tabs-mode nil)

(add-hook 'speedbar-mode-hook
          (lambda()
            (speedbar-add-supported-extension "\\.rb")
            (speedbar-add-supported-extension "\\.ru")
            (speedbar-add-supported-extension "\\.erb")
            (speedbar-add-supported-extension "\\.rjs")
            (speedbar-add-supported-extension "\\.rhtml")
            (speedbar-add-supported-extension "\\.rake")
            (speedbar-add-supported-extension "\\.haml")
            (speedbar-add-supported-extension "\\.php")
            (speedbar-add-supported-extension "\\.py")
            (speedbar-add-supported-extension "\\.xml")
            (speedbar-add-supported-extension "\\.yml")))

(setq browse-url-browser-function 'browse-url-generic
      browse-url-generic-program "google-chrome")

;;(setenv "PATH" (concat (getenv "PATH") ":"))
;;(exec-path-from-shell-copy-env "PATH")
(add-to-list 'exec-path "/usr/local/bin")

;; OS X support
(when (eq system-type 'darwin) ;; mac specific settings
  (setq mac-option-modifier 'control)
  (setq mac-command-modifier 'meta)
  (global-set-key [kp-delete] 'delete-char) ;; sets fn-delete to be right-delete
  )


;;(setq mac-command-modifier 'control)
(ido-mode 1)

(setq column-number-mode t)

(add-hook 'ruby-mode-hook (lambda () (highlight-indentation-current-column-mode)))
(add-hook 'ruby-mode-hook
          (lambda ()
            (fci-mode)
            (setq fci-rule-column 90)))

(add-hook 'python-mode-hook
          (lambda ()
            (fci-mode)
            (setq fci-rule-column 80)))


;; display any item that contains the chars you typed
(setq ido-enable-flex-matching t)

(setq-default c-basic-offset 4)

(global-set-key (kbd "C-c C-w") #'whitespace-mode)
(global-set-key (kbd "C-c C-m") #'magit-status)

(global-set-key (kbd "C-x b")   #'helm-mini)
(global-set-key (kbd "C-x C-m") #'helm-M-x)
(global-set-key (kbd "C-x C-r") #'helm-recentf)
(global-set-key (kbd "C-x r l") #'helm-filtered-bookmarks)
(global-set-key (kbd "M-y")     #'helm-show-kill-ring)
(global-set-key (kbd "M-s o")   #'helm-swoop)
(global-set-key (kbd "M-s /")   #'helm-multi-swoop)
(global-set-key (kbd "M-s s")   #'helm-ag)
(global-set-key (kbd "C-c C-f") #'helm-projectile)

(global-set-key (kbd "C-c C-t")   #'sr-speedbar-open)
(global-set-key (kbd "C-c C-q")   #'sr-speedbar-close)

(global-set-key (kbd "C-c C-n")   #'nav)

(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)

(add-hook 'after-init-hook 
          (lambda ()
            (load "~/.emacs.d/rc/yasnippet-rc.el")
            (load "~/.emacs.d/rc/org-mode-rc.el")
            (load "~/.emacs.d/rc/helm-gtags-rc.el")
            (load "~/.emacs.d/rc/go-rc.el")
            (load "~/.emacs.d/rc/docker-rc.el")))

(put 'downcase-region 'disabled nil)

(setq ruby-deep-indent-paren nil)

(when (require 'ansi-color nil t)
  (defun my-colorize-compilation-buffer ()
    (when (eq major-mode 'compilation-mode)
      (ansi-color-apply-on-region compilation-filter-start (point-max))))
  (add-hook 'compilation-filter-hook 'my-colorize-compilation-buffer))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(json-navigator json-reformat json-mode magit-todos go-autocomplete go-mode gore-mode ac-racer cargo company-racer racer bitbake yaml-mode cython-mode flycheck-cython cmake-mode highlight-indent-guides multiple-cursors helm-gtags highlight highlight-symbol magit yasnippet yasnippet-snippets helm helm-swoop))
 '(safe-local-variable-values
   '((eval setq-local compile-command "docker exec -it chennal  bash -c \"cd /src; ass build\"")
     (eval setq-local compile-command "docker exec -it channel  bash -c \"cd /src; ass build\"")
     (eval setq-local compile-command "docker exec -it vis  bash -c \"cd /src; ass test\""))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
